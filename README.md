# Démarrage

Changer dans la partie main.tf -> les infos écrites en dur pour le backend :

- Penser à se connecter à az login !
- Le groupe créé : resource_group_name
- Le stockage choisit
- Le stockage doit posséder un conteneur contenant la valeur "tfstate", car indiqué dans le "container_name"

Démarrer docker :

- docker compose up -d
- docker exec -it infra_td_terraform_19_sept bash

# Commande

- terraform init
  - Avec les config backend :
    - terraform init -backend-config="resource_group_name=TO DO" -backend-config="storage_account_name=TO DO" -backend-config="container_name=tfstate" -backend-config="key=terraform.tfstate"
- terraform plan
- terraform apply -auto-approve
- terraform validate
- terraform fmt

# Inspecter l'état

- terraform console

# Supprimer l'état

- terraform state rm random_id.storage

# Environnements

- Générer un fichier terraform.tfvars

Exemple :

```
resource_group_name = "19-sept"
prefix = "jl"
environment = "staging"
```

# Informations complémentaires

- Block dynamique
  - https://developer.hashicorp.com/terraform/language/expressions/dynamic-blocks
- Modules
  - https://developer.hashicorp.com/terraform/language/modules/syntax
