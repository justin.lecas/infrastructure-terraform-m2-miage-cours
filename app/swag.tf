resource "azurerm_application_insights" "swag" {
  name                = var.insights_swag_name
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  application_type    = "web"

  tags = {
    environment = var.environment
  }
}

resource "azurerm_static_site" "swag" {
  name                = var.statics_swag_name
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  sku_tier            = "Free"
  sku_size            = "Free"
}

# Il faut configurer à la main les fichiers disponibles