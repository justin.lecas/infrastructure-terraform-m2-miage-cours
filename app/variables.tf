variable "resource_group_name" {
  type = string
}

variable "resource_group_location" {
  type    = string
  default = "West Europe"
}

variable "prefix" {
  type = string
}

variable "environment" {
  type = string
}

variable "insights_swag_name" {
	type = string
}

variable "statics_swag_name" {
	type = string
}

variable "service_plan_webapp" {
	type = string
}

variable "linux_plan_webapp" {
	type = string
}