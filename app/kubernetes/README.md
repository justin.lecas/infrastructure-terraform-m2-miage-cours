# Connexion

- az login
- az account set --subscription <token>
- az aks get-credentials --resource-group <resource-group> --name <nom-du-service>
- kubelogin convert-kubeconfig -l azurecli

# Manager les clusters Kubectl

- kubectl get deployments --all-namespaces=true
- kubectl create ns dev
- kubectl config set-context --current --namespace=dev
- kubectl get all --all-namespaces
- kubectl port-forward po/nginx 3000:80
- kubectl apply -f service.yaml
- kubectl port-forward svc/nervice 3000:3000 --address=0.0.0.0
- kubectl scale deployment nginx-deployment --replicas=10
- kubectl top node
- kubectl apply -f nginx.yaml
- kubectl get endpoints

# Helm

- NAMESPACE=ingress-basic
- helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
- helm repo update
- helm install ingress-nginx ingress-nginx/ingress-nginx \
  --create-namespace \
  --namespace $NAMESPACE \
  --set controller.service.annotations."service\.beta\.kubernetes\.io/azure-load-balancer-health-probe-request-path"=/healthz

Tester :

- kubectl --namespace ingress-basic get services -o wide -w ingress-nginx-controller

Déployer des fichiers tests :

- cd kubernetes/ingress-exemple
- kubectl apply -f aks-helloworld-one.yaml --namespace ingress-basic
- kubectl apply -f aks-helloworld-two.yaml --namespace ingress-basic
- kubectl apply -f hello-world-ingress.yaml --namespace ingress-basic

Nettoyer les fichiers depuis le namespace :

- kubectl delete namespace ingress-basic
