resource "azurerm_service_plan" "webapp" {
  name                = var.service_plan_webapp
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  os_type             = "Linux"
  sku_name            = "B1"
}

resource "azurerm_linux_web_app" "webapp" {
  name                = var.linux_plan_webapp
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_service_plan.webapp.location
  service_plan_id     = azurerm_service_plan.webapp.id

  site_config {
		application_stack {
			docker_image_name = "nginx:latest"
			docker_registry_url = "https://docker.io/library"
		}
	}
}